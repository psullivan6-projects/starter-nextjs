import styled from 'styled-components';

export const List = styled.ul`
  margin: 0;
  padding: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  font-weight: 500;
  font-size: 1rem;
  list-style: none;
  line-height: 1.5;
`;

export const ListItem = styled.li`
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }

  a {
    text-decoration: inherit;
  }
`;
