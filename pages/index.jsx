import Layout from 'Components/Layout'
import Link from 'next/link'
import Axios from 'axios';

// Styles
import { List, ListItem } from './index/styles';

const Index = ({ episodes }) => (
  <Layout>
    <h1>Batman TV Shows</h1>
    <List>
      {
        episodes.map((episode) => {
          return (
            <ListItem key={episode.id}>
              <Link as={`/p/${episode.id}`} href={`/post?id=${episode.id}`}>
                <a>{episode.name}</a>
              </Link>
            </ListItem>
          );
        })
      }
    </List>
  </Layout>
)

Index.getInitialProps = async function() {
  const { data } = await Axios.get('https://api.tvmaze.com/shows/1871/episodes');

  console.log(`Show data fetched. Count: ${data.length}`)

  return {
    episodes: data
  }
}

export default Index